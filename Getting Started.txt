


Reviewing the map editor workflow:

- Download Tiled. This is what we will be using for creating the map.
- Go to project Gdx-MyExamples-master, and open gdx-tiled-draw-map in Android Studio.
- Install dependencies (Build tools 28.0.3) if necessary.
- Review the source code (it's not that long) at core > src > ....
- Build and run the app.
- Open android\assets\level.tmx in Tiled
- Modify it in any way
- Run the app again.
- Verify that it changed.

Input:
- Open ControllerDemo-master in Android studio
- Install dependencies if necessary
- Run the app
- Verify that the on-screen controller works and moves the square.